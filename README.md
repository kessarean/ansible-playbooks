# ansible-playbooks

Learning Ansible, using this to document my playbooks and various notes. 

## Command examples

Run a shell command with ansible using regex (as noted by the `~`) and escalate to root via sudo. 

```
ansible '~ovirt-node0[2-3]' -i inventory.ini -m shell -a "whoami" --become
```

Similarly, run a playbook with regex. (overrides 'hosts' in the playbook)

```
ansible-playbook -i inventory.ini ovirt-installs/playbook_install_cockpit.yaml --limit '~ovirt-node0[2-3]'
```

If you want to see what you are targeting, `--list-hosts` will show you. You can add this to the above two commands as well. 

```
ansible-playbook -i inventory.ini nmcli/playbook_configure_dns.yaml --list-hosts

playbook: nmcli/playbook_configure_dns.yaml

  play #1 (all): all    TAGS: []
    pattern: ['all']
    hosts (7):
      dns02.studylab.local
      ovirt-engine.studylab.local
      dns01.studylab.local
      nfs.studylab.local
      ovirt-node01.studylab.local
      ovirt-node02.studylab.local
      ovirt-node03.studylab.local
```
